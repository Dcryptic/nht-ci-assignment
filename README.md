# Continuous Integration Assignment

The shared repo contains all the required references, scripts and example pipelines.
Please go through the Assignment.md file for details about the assignment.
On classroom, you need to submit your repo link and commit id.

Session Slides - https://docs.google.com/presentation/d/1Di4u7RIajhNzZ_TxIvNAz3utHPPT2ylvj_5HuIzE-PE/edit?usp=sharing

---

## Commits

1. For Gitlab Runner and Jenkins Pipeline, for both
   - Commit ID - `2c677911a4996b8aa9f7a2bbc5e5cc451a8c44ba`
   - Above commit ID ran successfully!!

---

## Approach

1. Jenkins is installed on a VM and configured to run on port 8080.
2. A new user is created on Jenkins with username `admin` and password `admin`.
   - Added gitlab and docker credentials for authentication
   - Added gitlab repo
3. A new job is created on Jenkins
   - webhook added on gitlab
   - docker image is built and pushed to docker hub
4. gitlab ci yaml was written
   - runner was created and registered/ran on VM
   - docker image is built and pushed to docker hub

---

## Screenshots

- Can be found on [screenshots](./screenshots/)
